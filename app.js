var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');


var createAccount = require('./routes/createAccount');
var createDoctor = require('./routes/createDoctor');
var createPatient = require('./routes/createPatient');
var createResearcher = require('./routes/createResearcher');
var addData = require('./routes/addData');
var getPatients = require('./routes/getPatients');
var getPatientData = require('./routes/getPatientData');
var getAllData = require('./routes/getAllData');
var login = require('./routes/login');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);
app.use('/createAccount',createAccount);
app.use('/createDoctor',createDoctor);
app.use('/createPatient',createPatient);
app.use('/createResearcher',createResearcher);
app.use('/addData',addData);
app.use('/getPatients',getPatients);
app.use('/getPatientData',getPatientData);
app.use('/getAllData',getAllData);
app.use('/login',login);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});



module.exports = app;
