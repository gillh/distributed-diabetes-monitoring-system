/**
 * Created by gillh on 2/7/2016.
 */
var express = require('express');
var db = require('../db.js');
var router = express.Router();

router.post('/',function(req,res,next){
    var user = {
        uname : req.param('uname'),
        pword : req.param('pword')
    };
    db.getUser(user,function(err,result){
        if(err){
            res.send(500);
        }
        if(result){
            res.send(result);
        }
    });
});

router.get('/', function(req, res, next) {
    var user = {
        uname : req.param('uname'),
        pword : req.param('pword')
    };
    db.getUser(user,function(err,result){
        if(err){
            res.send(500);
        }
        if(result){
            res.send(result);
        }
    });
});

module.exports = router;