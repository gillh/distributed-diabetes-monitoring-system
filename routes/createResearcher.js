/**
 * Created by gillh on 2/12/2016.
 */
var express = require('express');
var db = require('../db.js');
var router = express.Router();


router.post('/',function(req,res,next){
    var r = {
        uname: req.param('uname'),
        pword: req.param('pword'),
        name: req.param('name')
    };
    db.addResearcher(r,function(err,results){
        if(err){
            throw err;
        }
        res.send(results);
    });
});

router.get('/', function(req, res, next) {
    var r = {
        uname: req.param('uname'),
        pword: req.param('pword'),
        name: req.param('name')
    };
    db.addResearcher(r,function(err,results){
        if(err){
            throw err;
        }
        res.send(results);
    });
});

module.exports = router;