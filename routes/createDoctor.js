/**
 * Created by gillh on 2/3/2016.
 */
var express = require('express');
var db = require('../db.js');
var router = express.Router();


router.post('/',function(req,res,next){
    var d = {
        uname: req.param('uname'),
        pword: req.param('pword'),
        name: req.param('name')
    };
    db.addDoctor(d,function(err,results){
        if(err){
            throw err;
        }
        res.send(results);
    });
});

router.get('/', function(req, res, next) {
    var d = {
        uname: req.param('uname'),
        pword: req.param('pword'),
        name: req.param('name')
    };
    db.addDoctor(d,function(err,results){
        if(err){
            throw err;
        }
        res.send(results);
    });
});

module.exports = router;
