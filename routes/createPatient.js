/**
 * Created by gillh on 2/3/2016.
 */
var express = require('express');
var db = require('../db.js');
var router = express.Router();


router.post('/',function(req,res,next){
    var patient ={
        uname: req.param('uname'),
        pword: req.param('pword'),
        name: req.param('name')
    };
    var doctor = {
        name: req.param('dName')
    };
    db.addPatient(patient,doctor,function(err,result){
        if(err) throw err;
        res.send(result);
    });
});

router.get('/', function(req, res, next) {
    var patient ={
        uname: req.param('uname'),
        pword: req.param('pword'),
        name: req.param('name'),
    };
    var doctor = {
        name: req.param('dName')
    };
    db.addPatient(patient,doctor,function(err,result){
        if(err) throw err;
        res.send(result);
    });
});

module.exports = router;