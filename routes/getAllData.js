/**
 * Created by gillh on 2/7/2016.
 */
var express = require('express');
var db = require('../db.js');
var router = express.Router();


router.post('/',function(req,res,next){
    var dataType ={
        dataType: req.param('dataType')
    };
    db.getAllData(dataType,function(err,result){
        if(err) throw err;
        res.send(result);
    });
});

router.get('/', function(req, res, next) {
    var dataType ={
        dataType: req.param('dataType')
    };
    db.getAllData(dataType,function(err,result){
        if(err) throw err;
        res.send(result);
    });
});

module.exports = router;
