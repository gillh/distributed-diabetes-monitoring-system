/**
 * Created by gillh on 2/7/2016.
 */
var mysql = require('mysql');

var pool = mysql.createPool({
    host:       'localhost',
    port:       3306,
    user:       'gillh',
    password:   'Happy123',
    database:   'ddms',
    connectionLimit: 10
});


exports.addDoctor = function(doctor,callback){
    var sql = "INSERT INTO doctors SET ?";
    executeQuery(sql,doctor,callback);
};

exports.addPatient = function(patient,doctor,callback){
    var sql_insertPatient = "INSERT INTO patients SET ?";
    executeQuery(sql_insertPatient,patient,function(err,result){
        if(err) throw err;
        if(result){
            var newPid = result.insertId;
            var sql_findDoctor = "SELECT * FROM doctors WHERE ?";
            executeQuery(sql_findDoctor,doctor,function(err,result){
                if(err) throw err;
                if(result){
                    var patient2doctor = {
                        pID: newPid,
                        dID: result[0].id
                    };
                    console.log(patient2doctor);
                    var sql_insertPatient2Doctor = "INSERT INTO patient2doctor SET ?";
                    executeQuery(sql_insertPatient2Doctor,patient2doctor,callback);
                }
            });
        }
    });
};

exports.addResearcher = function(researcher,callback){
    var sql = "INSERT INTO researchers SET ?";
    executeQuery(sql,researcher,callback);
};

exports.addData = function(record,callback){
    var sql_findPatient = "SELECT * FROM patients WHERE ?";
    var patient = {
        name: record.name
    };
    executeQuery(sql_findPatient,patient,function(err,result){
        var patientData = {
            pID: result[0].id,
            dataType: record.dataType,
            reading: record.reading,
            units: record.units
        };
        var sql_insertData = "INSERT INTO patientdata SET ?";
        executeQuery(sql_insertData,patientData,callback);
    });

};

exports.getPatientData = function(patient,callback){
    var sql_getPatient = "SELECT * FROM patients WHERE ?";
    executeQuery(sql_getPatient,patient,function(err,result){
        if(err) throw err;
        if(result){
            var resultPid = result[0].id;
            console.log(resultPid);
            var patientId = {
                pID: resultPid
            };
            var sql_getPatientData = "SELECT * FROM patientdata WHERE ?";
            executeQuery(sql_getPatientData,patientId,callback);
        }
    });

};

exports.getAllData = function(dataType,callback){
    var sql_getAllData = "SELECT id,dataType,reading,units,insertdatetime FROM patientdata WHERE ?";
    executeQuery(sql_getAllData,dataType,callback);
};

exports.getPatients = function(doctor,callback){
    var sql_getPatients = "SELECT p.id, p.uname,p.pword,p.name FROM doctors d, patient2doctor pd, patients p WHERE d.id = pd.dID AND p.id = pd.pID AND d.name = '"+doctor.name+"';";
    executeQueryStrict(sql_getPatients,callback);
};

exports.getUser = function(user,callback){
    var tables = ['patients','doctors','researchers'];

    function checkLogin(i){
        var found = false;
        if(i<tables.length) {
            var sql = getSQLFindUser(tables,i,user.uname,user.pword);
            console.log(sql);
            executeQueryStrict(sql, function (err, res) {
                // Only return an error if we're at the end of the line
                if (err && !found && i == tables.length - 1) {
                    console.log(i);
                    callback(true);
                }
                // Only respond successfully if we haven't error'd (we have a response) and the response has content to return
                else if (res && res[0]) {
                    res[0].userType = i;
                    callback(false, res);
                    found = true;
                } 
                // Only start the next query if the previous has finished
                else {
                    if(!found){
                        checkLogin(i+1);
                    }
                }
            });
        }
    }

    function getSQLFindUser(tables,index,uname,pword){
        return "SELECT * FROM "+tables[index]+" WHERE uname = '"+uname+"' AND pword = '"+pword+"';";
    }

    checkLogin(0,tables,user.uname,user.pword);
};

function executeQuery(sql,object,callback){
    pool.getConnection(function(err,connection){
        if(err) {
            console.log(err);
            callback(true);
        }
        connection.query(sql,object,function(err,results){
            connection.release();
            if(err){
                console.log(err);
            }
            callback(false,results);
        });
    });
}

function executeQueryStrict(sql,callback){
    pool.getConnection(function(err,connection){
        if(err) {
            console.log(err);
            callback(true);
        }
        connection.query(sql,function(err,results){
            connection.release();
            if(err){
                throw err;
                console.log(err);
            }
            if(results[0]) {
                callback(false, results);
            } else {
                // No errors but no data; couldn't find what we're looking for
                callback(true);
            }
        });
    });
}

function executeQueryNoCallback(sql){
    var res= {};
    res = pool.getConnection(function(err,connection){
        if(err) {
            console.log(err);
            throw err;
        }

        return connection.query(sql,function(err,results){
            connection.release();
            if(err){
                console.log(err);
            }
            return JSON.stringify(results);
        });

    });
    console.log(res);
    return res;
}

